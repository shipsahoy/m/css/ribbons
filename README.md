# *Fork me on GitLab* CSS ribbon

This is a fork of the [Fork me on GitHub CSS ribbon](https://github.com/simonwhitaker/github-fork-ribbon-css)
, a recreation of the Fork me on GitHub ribbon in CSS, hence resolution-independent.

See it in action [here](https://alvarolopezborr.gitlab.io/gitlab-fork-ribbon-css/)

Feel free to fork, tweak and send me a pull request.

Note: this project is not sponsored or in any way endorsed by GitLab, or by simonwhitaker.
